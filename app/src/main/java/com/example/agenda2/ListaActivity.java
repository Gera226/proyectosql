package com.example.agenda2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import com.example.agenda2.database.AgendaContactos;
import com.example.agenda2.database.contacto;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class ListaActivity extends android.app.ListActivity {

    private MyArrayAdapter adapter;
    private ArrayList<contacto> listaContacto;
    private AgendaContactos agendaContactos;
    private Button btnNuevo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);
        btnNuevo = (Button) findViewById(R.id.btNuevo);
        agendaContactos = new AgendaContactos(this);

        llenarLista();
        adapter = new MyArrayAdapter(this,R.layout.layout_contacto,listaContacto);
        String str = adapter.contactos.get(1).getNombre();
        setListAdapter(adapter);

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void llenarLista(){
        agendaContactos.openDataBase();
        listaContacto = agendaContactos.allContactos();
        agendaContactos.cerrar();
    }


    class MyArrayAdapter extends ArrayAdapter<contacto> {
        private Context context;
        private int textViewResourceId;
        private ArrayList<contacto> contactos;

        public MyArrayAdapter(Context context, int resource, ArrayList<contacto> contactos) {
            super(context, resource);
            this.context = context;
            this.textViewResourceId = resource;
            this.contactos = contactos;
        }

        public View getView(final int position, View converView, ViewGroup viewGroup) {
            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewResourceId, null);


            TextView lblNombreContacto = (TextView) view.findViewById(R.id.lblNombreContacto);
            TextView lblTelefonoContacto = (TextView) view.findViewById(R.id.lblTelefonoContacto);

            Button btnModificar = (Button) view.findViewById(R.id.btnModificar);
            Button btnBorrar = (Button) view.findViewById(R.id.btnBorrar);

            if (contactos.get(position).getFavorito() > 0) {
                lblNombreContacto.setTextColor(Color.BLUE);
                lblTelefonoContacto.setTextColor(Color.BLUE);
            } else {
                lblNombreContacto.setTextColor(Color.BLACK);
                lblTelefonoContacto.setTextColor(Color.BLACK);
            }
            lblNombreContacto.setText(contactos.get(position).getNombre());
            lblTelefonoContacto.setText(contactos.get(position).getTelefono1());

            btnBorrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    agendaContactos.openDataBase();
                    agendaContactos.deleteContacto(contactos.get(position).get_ID());
                    agendaContactos.cerrar();
                    contactos.remove(position);
                    notifyDataSetChanged();
                }


            });

            btnModificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("contacto", contactos.get(position));
                    Intent i = new Intent();
                    i.putExtras(bundle);
                    setResult(Activity.RESULT_OK,i);
                    finish();
                }
            });
            return view;
        }
    }
}



