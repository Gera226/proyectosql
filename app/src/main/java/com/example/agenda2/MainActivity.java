package com.example.agenda2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.agenda2.database.AgendaContactos;
import com.example.agenda2.database.AgendaDbHelper;
import com.example.agenda2.database.DefinirTabla;
import com.example.agenda2.database.contacto;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    final ArrayList<contacto> contactos = new ArrayList<contacto>();
    private EditText txtNombre;
    private EditText txtTel;
    private EditText txtTelefono2;
    private EditText txtDomicilio;

    private EditText txtNotas;
    private CheckBox cbxFavorito;
    private contacto savedContact;

    private Button   btnSalir;
    private AgendaContactos db;
    // private int savedIndex;
    long id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtTel = (EditText) findViewById(R.id.txtTel1);
        txtTelefono2 = (EditText) findViewById(R.id.txtTel2);
        txtDomicilio = (EditText) findViewById(R.id.txtDomicilio);
        txtNotas = (EditText) findViewById(R.id.txtNotas);
        cbxFavorito = (CheckBox) findViewById(R.id.cbFavorito);
        Button btnGuardar = (Button) findViewById(R.id.btnGuardar);
        Button btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        Button btnListar = (Button) findViewById(R.id.btnLista);
        Button btnSalir = (Button) findViewById(R.id.btnSalir);

        db = new AgendaContactos(MainActivity.this);


        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtNombre.getText().toString().equals("") || txtDomicilio.getText().toString().equals("") || txtTel.getText().toString().equals("")) {
                    Toast.makeText(MainActivity.this, R.string.mensajeerror, Toast.LENGTH_SHORT).show();
                } else {
                    contacto nContacto = new contacto();
                    nContacto.setNombre(txtNombre.getText().toString());
                    nContacto.setTelefono1(txtTel.getText().toString());
                    nContacto.setTelefono2(txtTelefono2.getText().toString());
                    nContacto.setDomicilio(txtDomicilio.getText().toString());
                    nContacto.setNotas(txtNotas.getText().toString());
                    if (cbxFavorito.isChecked()){
                        nContacto.setFavorito(1);
                    }else{
                        nContacto.setFavorito(0);
                    }
                    db.openDataBase();

                    if (savedContact==null){
                        long idx=db.insertarContacto(nContacto);
                        Toast.makeText(MainActivity.this,"Se agrego Contacto con ID"+idx,Toast.LENGTH_SHORT).show();
                    }else {
                        db.UpdateContactos(nContacto,id);
                        Toast.makeText(MainActivity.this,"Se agrego Contacto con ID"+id,Toast.LENGTH_SHORT).show();
                    }
                    db.cerrar();
                }
            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,ListaActivity.class);
                startActivityForResult(i,0);
            }
        });


        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

    }

    public void limpiar() {
        savedContact = null;
        txtNombre.setText("");
        txtTel.setText("");
        txtTelefono2.setText("");
        txtDomicilio.setText("");
        txtNotas.setText("");
        cbxFavorito.setChecked(false);
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Activity.RESULT_OK == resultCode) {
            contacto contacto = (contacto) data.getSerializableExtra("contacto");
            savedContact = contacto;
            id = contacto.get_ID();
            txtNombre.setText(contacto.getNombre());
            txtTel.setText(contacto.getTelefono1());
            txtTelefono2.setText(contacto.getTelefono2());
            txtDomicilio.setText(contacto.getDomicilio());
            txtNotas.setText(contacto.getNotas());
            if (contacto.getFavorito() > 0) {
                cbxFavorito.setChecked(true);
            }
        } else {
            limpiar();
        }
    }


}
