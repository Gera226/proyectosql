package com.example.agenda2.database;

import java.io.Serializable;

public class contacto implements Serializable {
    private long _ID;
    private String nombre;
    private String telefono1;
    private String telefono2;
    private String domicilio;
    private String notas;
    private int favorito;

    public contacto(long _ID, String nombre, String telefono1, String telefono2, String domicilio, String notas, int favorito) {
        this._ID = _ID;
        this.nombre = nombre;
        this.telefono1 = telefono1;
        this.telefono2 = telefono2;
        this.domicilio = domicilio;
        this.notas = notas;
        this.favorito = favorito;
    }



    public contacto() {
        this._ID = 0;
        this.nombre = "";
        this.telefono1 = "";
        this.telefono2 = "";
        this.domicilio = "";
        this.notas = "";
    }

    public contacto(contacto c) {
        this._ID = c.get_ID();
        this.nombre = c.getNombre();
        this.telefono1 = c.getTelefono1();
        this.telefono2 = c.getTelefono2();
        this.domicilio = c.getDomicilio();
        this.notas = c.getNotas();
    }

    public long get_ID() {
        return _ID;
    }

    public String getNombre() {
        return nombre;
    }

    public String getTelefono1() {
        return telefono1;
    }

    public String getTelefono2() {
        return telefono2;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public String getNotas() {
        return notas;
    }

    public int getFavorito() {
        return favorito;
    }

    public void set_ID(long _ID) {
        this._ID = _ID;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setTelefono1(String telefono1) {
        this.telefono1 = telefono1;
    }

    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public void setFavorito(int favorito) {
        this.favorito = favorito;
    }
}
