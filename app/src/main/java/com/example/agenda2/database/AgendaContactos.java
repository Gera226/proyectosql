package com.example.agenda2.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class AgendaContactos {
    private Context context;
    private AgendaDbHelper agendaDbHelper;
    private SQLiteDatabase db;

    private String[] columnToRead = new String[]{
            DefinirTabla.Contacto._ID, DefinirTabla.Contacto.NOMBRE, DefinirTabla.Contacto.TELEFONO1, DefinirTabla.Contacto.TELEFONO2,
            DefinirTabla.Contacto.DOMICILIO, DefinirTabla.Contacto.NOTAS, DefinirTabla.Contacto.FAVORITO
    };

    public AgendaContactos(Context context) {
        this.context = context;
        agendaDbHelper = new AgendaDbHelper(this.context);
    }

    public void openDataBase(){

        db= agendaDbHelper.getWritableDatabase();
    }

    public long insertarContacto (contacto c){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Contacto.NOMBRE, c.getNombre());
        values.put(DefinirTabla.Contacto.TELEFONO1, c.getTelefono1());
        values.put(DefinirTabla.Contacto.TELEFONO2, c.getTelefono2());
        values.put(DefinirTabla.Contacto.DOMICILIO, c.getDomicilio());
        values.put(DefinirTabla.Contacto.NOTAS, c.getNotas());
        values.put(DefinirTabla.Contacto.FAVORITO, c.getFavorito());

        return db.insert(DefinirTabla.Contacto.TABLA_NAME, null,values);
    }

    public long UpdateContactos (contacto c, long id){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Contacto.NOMBRE, c.getNombre());
        values.put(DefinirTabla.Contacto.TELEFONO1, c.getTelefono1());
        values.put(DefinirTabla.Contacto.TELEFONO2, c.getTelefono2());
        values.put(DefinirTabla.Contacto.DOMICILIO, c.getDomicilio());
        values.put(DefinirTabla.Contacto.NOTAS, c.getNotas());
        values.put(DefinirTabla.Contacto.FAVORITO, c.getFavorito());

        String criterio = DefinirTabla.Contacto._ID + " = " + id;
        return db.update(DefinirTabla.Contacto.TABLA_NAME, values,criterio, null);
    }

    public int deleteContacto (long id){
        String criterio = DefinirTabla.Contacto._ID + " = " + id;

        return db.delete(DefinirTabla.Contacto.TABLA_NAME, criterio, null);
    }

    public contacto readContacto(Cursor cursor){
        contacto c = new contacto();
        c.set_ID(cursor.getInt(0));
        c.setNombre(cursor.getString(1));
        c.setTelefono1(cursor.getString(2));
        c.setTelefono2(cursor.getString(3));
        c.setDomicilio(cursor.getString(4));
        c.setNotas(cursor.getString(5));
        c.setFavorito(cursor.getInt(6));
        return c;
    }

    public contacto getContacto(long id){
        contacto contacto = null;
        SQLiteDatabase db = agendaDbHelper.getReadableDatabase();
        Cursor c= db.query(DefinirTabla.Contacto.TABLA_NAME, columnToRead, DefinirTabla.Contacto._ID + " = ? ", new String[] {String.valueOf(id)}, null,null,null);

        if(c.moveToFirst()){
            contacto = readContacto(c);
        }


        c.close();
        return contacto;
    }


    public ArrayList<contacto> allContactos(){

        ArrayList<contacto> contactos = new ArrayList<contacto>();

        Cursor cursor = db.query(DefinirTabla.Contacto.TABLA_NAME, null,null,null,null,null,null);

        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            contacto c = readContacto(cursor);
            contactos.add(c);
            cursor.moveToNext();
        }
        cursor.close();
        return contactos;
    }

    public void cerrar(){
        agendaDbHelper.close();
    }

}

